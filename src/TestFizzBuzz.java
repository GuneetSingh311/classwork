import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;



class TestFizzBuzz {

	/* Create a function that returns fizz,buzz, fizzbuzz.
	 1.* Your function should accept numbr >0
	 --> what should happen if number <0?
	 * the func should:
	 2.* div/3 -> return fizz
	 3.* div/5 -> return buzz
	 4.* div/15 - > return fizzbuzz
	 5.* return number if no other requiremrnt is fullfilled.
	 * 
	 */
	// stub: getFizzBuzz(number)
	
	
	@Test
	void testNumberGreaterThanZero() {
		// Req.1: red- getFizzBuzz() not implemented.
		FizzBuzz fb = new FizzBuzz();
		String rs = fb.getFizzBuzz(1);
		assertEquals("1",rs);
		}
	
	@Test 
	void testInputLessThanZero() {
		// Req1- red phase --> testing when input is <0
		FizzBuzz fb = new FizzBuzz();
		String rs = fb.getFizzBuzz(-5);
		assertEquals("error",rs);
	}
	
	
	/// ----------- Test case for Req:3
	
	@ Test
	void testinputDivisbleBy5() {
		// Req3. Red- test case phase when inputing when a number is divisble by 5.
		FizzBuzz fb = new FizzBuzz();
		String rs = fb.getFizzBuzz(10);
		assertEquals("buzz",rs);
		
	}
	
	/// -------------- Test case for Req2:
	
	
	@Test 
	void testinputDivisbleBy3() {
		// Req2: Green case: test passes input number divisible by 3.
		FizzBuzz fb = new FizzBuzz();
		String rs = fb.getFizzBuzz(18);
		assertEquals("fizz",rs);
		
}
	
	////////////// Test case for req4.
	@ Test
	void testinputDivisibleBy15() {
		// Req4: Red phase- test case fails when number divisble by 15.
		FizzBuzz fb = new FizzBuzz();
		String rs = fb.getFizzBuzz(90);
		assertEquals("FizzBuzz",rs);
	}
	
	///////////////// Return number of no req is met.
	
	
	@Test 
	void testInputIsSomethingElse() {
		// Req5: Red phase test case fails.
		FizzBuzz fb = new FizzBuzz();
		String rs  = fb.getFizzBuzz(7);
		assertEquals("7",rs);
	}
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	

}
