

public class FizzBuzz {

	public String getFizzBuzz(int number) {

		// What is the differene between int and Integer().
		// int x = 5; Primitive
		// Integer m = new Integer(5); Object
		
		if (number <0)
		{
		return "error";
	}
		// REQ:4 minimum test code to fix all test case fail
		// In this solution testing helps in getting the order.
		// Refactor:- remove duplicate code.
		else if (number%15==0) {
			return "FizzBuzz";
		}
		
		if (number%5==0){

			return "buzz";
		}
		else if (number%3==0) {
			
			return "fizz";
		}
		
		// Req5. to fix failing test case
		// This line is causing the problem!
		// Solution: convert an int to a String
		//(a primitive int, not an Integer Object)
	
		return String.valueOf(number);

	}
	
	
	
	
}
